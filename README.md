# redir

learning experiments in ApiGateway/labmda/dynamodb with terraform

## intents
* play with dynamodb
* play with terraform to create multiple resources from a collection
* understand what http header data is recieved by a lambda
* lastly, explore the practicality of setting up multiple redirection hostnames

## lambda test pages

* https://disney.mergedamage.com
* https://apple.mergedamage.com
* https://gitlab.mergedamage.com
* https://nintendo.mergedamage.com
* https://google.mergedamage.com