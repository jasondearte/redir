resource "aws_dynamodb_table" "redirects" {
  name           = "redirects"
  read_capacity  = 2
  write_capacity = 2
  hash_key       = "hostname"

  attribute = [
    {
      name = "hostname"
      type = "S"
    },
  ]
}

data "template_file" "dbitem" {
  template = <<ITEM
{
  "hostname": {
    "S": "$${src}"
  },
  "redirect": {
    "S": "$${dst}"
  }
}
ITEM

  vars {
    src = "${lookup(var.redirects[count.index], "hostname")}"
    dst = "${lookup(var.redirects[count.index], "redirect")}"
  }

  count = "${length(var.redirects)}"
}

resource "aws_dynamodb_table_item" "multiple" {
  table_name = "${aws_dynamodb_table.redirects.name}"
  hash_key   = "${aws_dynamodb_table.redirects.hash_key}"
  item       = "${element(data.template_file.dbitem.*.rendered, count.index)}"
  count      = "${length(var.redirects)}"
}
