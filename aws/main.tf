locals {
  domain_name         = "mergedamage.com"
  function_name       = "redir"
  function_handler    = "main.handler"
  function_runtime    = "nodejs6.10"
  gateway_description = "redirection lambda"
}

# Pre-existing certificate for our domain front end to the lambda
data "aws_acm_certificate" "cert" {
  domain   = "*.${local.domain_name}"
  statuses = ["ISSUED"]
}

resource "aws_api_gateway_domain_name" "site" {
  domain_name     = "${lookup(var.redirects[count.index], "hostname")}.${local.domain_name}"
  certificate_arn = "${data.aws_acm_certificate.cert.arn}"

  count = "${length(var.redirects)}"
}

data "aws_route53_zone" "site" {
  name         = "${local.domain_name}."
  private_zone = false
  provider     = "aws.global"
}

# dns name for all the redirects that direct to the api_gateway
resource "aws_route53_record" "site" {
  zone_id = "${data.aws_route53_zone.site.id}"
  name    = "${lookup(var.redirects[count.index], "hostname")}"
  type    = "A"

  alias {
    name                   = "${element(aws_api_gateway_domain_name.site.*.cloudfront_domain_name, count.index)}"
    zone_id                = "${element(aws_api_gateway_domain_name.site.*.cloudfront_zone_id, count.index)}"
    evaluate_target_health = true
  }

  count = "${length(var.redirects)}"
}

# Where the lambda will be stored
data "aws_s3_bucket" "bucket" {
  bucket = "mergedamagetfs"
}

# upload the lambda to the bucket
resource "aws_s3_bucket_object" "lambda" {
  bucket = "${data.aws_s3_bucket.bucket.id}"
  key    = "lambda-redir/${var.package_keyname_lammy}"
  source = "${var.package_filename_lammy}"
  etag   = "${md5(file("${var.package_filename_lammy}"))}"
}

# permission to run the lambda
resource "aws_iam_role" "lambda_exec" {
  name = "${local.function_name}-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# Defines the lambda's name, s3 source location, entrypoint and role
resource "aws_lambda_function" "lambda" {
  function_name = "${local.function_name}"
  s3_bucket     = "${aws_s3_bucket_object.lambda.bucket}"
  s3_key        = "${aws_s3_bucket_object.lambda.key}"
  role          = "${aws_iam_role.lambda_exec.arn}"
  handler       = "${local.function_handler}"
  runtime       = "${local.function_runtime}"

  environment {
    variables = {
      REGION = "us-east-1"
    }
  }
}

# Provides an API Gateway REST API
resource "aws_api_gateway_rest_api" "lambda" {
  name        = "${local.function_name}"
  description = "${local.gateway_description}"
}

# Create the API Gateway
resource "aws_api_gateway_resource" "proxy" {
  rest_api_id = "${aws_api_gateway_rest_api.lambda.id}"
  parent_id   = "${aws_api_gateway_rest_api.lambda.root_resource_id}"
  path_part   = "{proxy+}"
}

# -------------------------------------------------------------------

# Provides a HTTP Method for an API Gateway Resource
resource "aws_api_gateway_method" "proxy" {
  rest_api_id   = "${aws_api_gateway_rest_api.lambda.id}"
  resource_id   = "${aws_api_gateway_resource.proxy.id}"
  http_method   = "ANY"
  authorization = "NONE"
}

# Provides an HTTP Method Integration for an API Gateway Integration.
resource "aws_api_gateway_integration" "lambda" {
  rest_api_id = "${aws_api_gateway_rest_api.lambda.id}"
  resource_id = "${aws_api_gateway_method.proxy.resource_id}"
  http_method = "${aws_api_gateway_method.proxy.http_method}"
  type        = "AWS_PROXY"
  uri         = "${aws_lambda_function.lambda.invoke_arn}"

  integration_http_method = "POST"
}

# -------------------------------------------------------------------

resource "aws_api_gateway_method" "proxy_root" {
  rest_api_id   = "${aws_api_gateway_rest_api.lambda.id}"
  resource_id   = "${aws_api_gateway_rest_api.lambda.root_resource_id}"
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambda_root" {
  rest_api_id = "${aws_api_gateway_rest_api.lambda.id}"
  resource_id = "${aws_api_gateway_method.proxy_root.resource_id}"
  http_method = "${aws_api_gateway_method.proxy_root.http_method}"
  type        = "AWS_PROXY"
  uri         = "${aws_lambda_function.lambda.invoke_arn}"

  integration_http_method = "POST"
}

# -------------------------------------------------------------------

resource "aws_api_gateway_deployment" "lambda" {
  depends_on = [
    "aws_api_gateway_integration.lambda",
    "aws_api_gateway_integration.lambda_root",
  ]

  rest_api_id = "${aws_api_gateway_rest_api.lambda.id}"
  stage_name  = "test"
}

# Connect {domain_name}/{base_path} -> Lambda
resource "aws_api_gateway_base_path_mapping" "lambda" {
  api_id      = "${aws_api_gateway_rest_api.lambda.id}"
  stage_name  = "${aws_api_gateway_deployment.lambda.stage_name}"
  domain_name = "${lookup(var.redirects[count.index], "hostname")}.${local.domain_name}"
  base_path   = ""

  count = "${length(var.redirects)}"
}

# Allow API Gateway to Access Lambda

resource "aws_lambda_permission" "lambda" {
  statement_id  = "APIGWInvoke-${local.function_name}"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda.arn}"
  principal     = "apigateway.amazonaws.com"

  # The /*/* portion grants access from any method on any resource
  # within the API Gateway "REST API".
  source_arn = "${aws_api_gateway_deployment.lambda.execution_arn}/*/*"
}
