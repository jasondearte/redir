# ../../terraform_backend project created the state bucket

provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = "$HOME/.aws/credentials"
}

provider "aws" {
  alias                   = "global"
  region                  = "us-east-1"
  shared_credentials_file = "$HOME/.aws/credentials"
}

terraform {
  backend "s3" {
    bucket         = "mergedamagetfs"
    key            = "terraformstate/mergedamageredir"
    region         = "us-east-1"
    dynamodb_table = "terraform-lock"
  }
}
