variable "redirects" {
  type        = "list"
  description = "list of all the redirect mappings"

  default = [
    {
      hostname = "disney"
      redirect = "disney.com"
    },
    {
      hostname = "apple"
      redirect = "apple.com"
    },
    {
      hostname = "gitlab"
      redirect = "gitlab.com"
    },
    {
      hostname = "nintendo"
      redirect = "nintendo.com"
    },
    {
      hostname = "google"
      redirect = "google.com"
    },
  ]
}

variable "package_filename_lammy" {
  type        = "string"
  description = "path to the zip containing the lambda, poplulated by the ci script"
}

variable "package_keyname_lammy" {
  type        = "string"
  description = "ci generated unique key for the lambda zip file"
}
