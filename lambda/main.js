'use strict';

var header = "<!DOCTYPE html>\n" +
    "<html lang=\"en\">\n" +
    "<head>\n" +
    "    <meta charset=\"utf-8\">\n" +
    "    <title>MergeDamage </title>\n" +
    "    <meta name=\"description\" content=\"merge damage\">\n" +
    "    <meta name=\"author\" content=\"Jason De Arte\">\n" +
    "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
    "    <link href=\"//fonts.googleapis.com/css?family=Raleway:400,300,600\" rel=\"stylesheet\" type=\"text/css\">\n" +
    "    <link rel=\"stylesheet\" href=\"//mergedamage.com/Skeleton-2.0.4/css/normalize.css\">\n" +
    "    <link rel=\"stylesheet\" href=\"//mergedamage.com/Skeleton-2.0.4/css/skeleton.css\">\n" +
    "</head>\n" +
    "<body>\n\t";

var footer = "\n</body></html>"

exports.handler = function (event, context, callback) {
    var response = {
        statusCode: 200,
        headers: {
            'Content-Type': 'text/html; charset=utf-8',
        },
        body: `${header}
<div class="container">
    <div class="row">
        <div class=" column" style="margin-top: 20px">
        
<h3>I think you want [${event.requestContext.domainPrefix}]?</h3>

<p>
test links<br/>
<a href="https://disney.mergedamage.com">disney.mergedamage.com</a> | 
<a href="https://apple.mergedamage.com">apple.mergedamage.com</a> | 
<a href="https://gitlab.mergedamage.com">gitlab.mergedamage.com</a> | 
<a href="https://nintendo.mergedamage.com">nintendo.mergedamage.com</a> | 
<a href="https://google.mergedamage.com">google.mergedamage.com</a>
</p>

<h4>event parameter</h4>
<pre><code>${JSON.stringify(event, null, 2)}</code></pre>

<h4>context parameter</h4>
<pre><code>${JSON.stringify(context, null, 2)}</code></pre>

        </div>
    </div>
</div>

${footer}`,
    };
    callback(null, response);
};
